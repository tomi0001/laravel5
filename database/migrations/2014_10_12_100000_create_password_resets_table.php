<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasswordResetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('login',60);
            $table->string('email');
            $table->integer('date_registration');
            $table->string('password',200);
            $table->string('permissions',1);
            $table->rememberToken();
            $table->timestamps();
        });
        
       Schema::create('statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('date');
            $table->string('variable_http_user_agent',200);
            $table->string('page',200);
            $table->string('ip',20);
            $table->string('user',60);
        });
        
       Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->integer('id_category');
            $table->integer('id_category2');
            $table->float('price');
            $table->text('description');
            $table->bool('auction');
            $table->bool('sold');
            $table->integer('id_usera');
            $table->integer('quantity');
            $table->string('path_to_image',100);
            
        });        
       Schema::create('basket', function (Blueprint $table) {
            $table->increments('id');
            $table->string('quantity_products',100);
            $table->integer('id_usera');
            
        });        
       Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            
        });        
       Schema::create('category2', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            
        });        

    }

    /**tabela products
id int
name varchar 100
id_category int
id_category2 int
price float
description text
auction bool
sold bool
id_usera int
quantity int
path_to_image varchar 100

tabela basket
id int
quantity_products varchar 100
id_usera int

tabela category
id int
name varchar 100

tabela category2
id int
name varchar 100
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        Schema::drop('statistics');
        Schema::drop('products');
        Schema::drop('basket');
        Schema::drop('category');
        Schema::drop('category2');
    }
}
