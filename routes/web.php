<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('registration','Controller@registration');
/*
Route::get('registration',function () {
  
  if (!Auth::check()) {
    return View::make('regi');
  }
  else {
  //print "dobrze";
    return View::make('blad');
  }

});

*/
Route::get('blad',function () { 
    return View::make('blad');
});
Route::get('/views_product','Controller@views_product');
Route::get('/views_product/{id?}','Controller@views_product2');


/*
Route::get('views_product',function () { 
    //return View::make('blad');
    //print "dobrze";
  
  
  $permissions = new \App\Http\Controllers\klasa();
  //$user = new User();
  //print Auth::user()->permissions;
  if (empty(Auth::User()->permissions)) {
    return Redirect::to('blad')->with('login_error','Nie masz dostępu do tej części strony');
  }
  else {
    $result = $permissions->check_permissions(Auth::user()->permissions,"views_product");
    if ($result == true) return View::make('views_product');
    else return Redirect::to('blad')->with('login_error','Nie masz dostępu do tej części strony');
  }

});
*/
/*
Route::get('wyloguj',function () { 
    Auth::logout();
    return View::make('regi');
    //print "dobrze";
  

});
*/
Route::get('add_product_succes','Controller@add_product_succes');
Route::get('wyloguj','Controller@index');
Route::post('/ajax/ajax2','Controller@ajax2');
Route::get('/ajax/ajax3','Controller@ajax3');
Route::get('/ajax/ajax4','Controller@ajax4');
Route::get('/ajax/{id?}','Controller@ajax');
Route::get('add_categories','Controller@add_categories');
Route::get('show_cate/{id?}','Controller@show_cate');
/*
Route::get('/ajax/{id?}',function ($id) {
  if (Auth::check()) {
    
    if (Request::ajax()) {
      //return View('/ajax/ajax',['id' => 1]);
      return View('/ajax/ajax')->with('id',$id);
    }
  }


});
*/
Route::get('add_product','Controller@add_product');
/*
Route::get('add_product',function () {
  $permissions = new \App\Http\Controllers\klasa();
  //$user = new User();
  //print Auth::user()->permissions;
  if (empty(Auth::User()->permissions)) {
    return Redirect::to('blad')->with('login_error','Nie masz dostępu do tej części strony');
  }
  else {
    $result = $permissions->check_permissions(Auth::user()->permissions,"add_product");
    if ($result == true) return View::make('add_product');
    else return Redirect::to('blad')->with('login_error','Nie masz dostępu do tej części strony');
  }


});
*/
Route::get('registration_succes',function () {

  return View::make('regi_succ');

});
Route::post('login','Controller@login');
/*
Route::post('login',function () {

 

  $user = array(
    'login' => Input::get('login'),
    'password' => Input::get('password')
  );
  if (Auth::attempt($user))
  {
    return Redirect::to('registration');
  }
  else {
    return Redirect::to('blad')->with('login_error','Nieprawidłowy login lub hasło');
  }



});
*/
Route::get('add_regi','Controller@add_regi');
/*
Route::get('add_regi',function () {

          $rules = array(
    'email' => 'required|email|unique:users',
    'password' => 'required|same:password2',
    'login'=> 'required|unique:users'
      );
      $validation = Validator::make(Input::all(), $rules);
      if ($validation->fails())
      {
	return Redirect::to('registration')->withErrors($validation)->withInput();
      }
      $user = new \App\User;
      $user->email = htmlspecialchars(Input::get('email'));
      $user->password = Hash::make(Input::get('password'));
      $user->login = htmlspecialchars(Input::get('login'));
      $user->date_registration = time();
      $user->permissions = 1;
      if ($user->save())
      {
	//Auth::loginUsingId($user->id);
	return Redirect::to('registration_succes');
      }	

});
*/
 
//Auth::routes();

Route::get('/home', 'HomeController@index');
