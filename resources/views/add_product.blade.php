@extends('layout.index')


@section('content')
<br>








{{Form::open(array('url' => '/ajax/ajax2', 'files' => true))}}

<div class=row>
<div class=col-xs-2>
</div>
<div class=col-xs-3>
<span class=form>Nazwa produktu</span>
</div>
<div class=col-xs-4>
<input type="text" class="form-control" name="name" value={{Input::old('name')}}>
</div>
</div>

<br>

<div class=row>
<div class=col-xs-2>

</div>
<div class=col-xs-3>
<span class=form>Opis produktu</span>
</div>
<div class=col-xs-4>
<textarea class="form-control" name=description rows=6>
</textarea>
</div>

</div>
<br>
<div class=row>
<div class=col-xs-2>

</div>
<div class=col-xs-3>

<span class=form>Nadrzędna kategoria</span>
</div>
<div class=col-xs-4>

<select id=category name =category class="form-control">
<option value= ></option>
<?php  
$i  =0;
$object_category = new \App\Http\Controllers\funkcje();

$table = $object_category->show_categories();
while ($i < count($table) ) {
  $id2 = $table[$i][0];
  $name = $table[$i][1];
  print "<option value=$id2>$name</option>";
  $i++;
}
?>
</select>
<a href=# id=categor >Kliknij dwa razy</a>
</div>
<div id=invisible></div>
</div>
<br>
<div class=row>
<div class=col-xs-2>
</div>
<div class=col-xs-3>
<span class=form>Podrzędna kategoria</span>
</div>
<div class=col-xs-4>

<select id=category2 name=category2 class="form-control"></select>
</div>



</div>

<br>
<div class=row>
<div class=col-xs-2>
</div>
<div class=col-xs-3>
<span class=form>Cena(jeżeli licytacja to od jakiej ceny)</span>
</div>
<div class=col-xs-4>

<div class="col-xs-4">
<input type="text" class="form-control" name="zl" placeholder=zł>
</div>
<div class="col-xs-4">
<input type="text" class="form-control" name="gr" placeholder=gr>
</div>
</div>





</div>



<br>
<div class=row>
<div class=col-xs-2>
</div>
<div class=col-xs-3>
<span class=form>Ilość</span>
</div>
<div class=col-xs-4>

<div class="col-xs-5">
<input type="text" class="form-control" name="quantity">
</div>
</div>




</div>




<br>
<div class=row>
<div class=col-xs-2>
</div>
<div class=col-xs-3>
<span class=form>Jeżeli licytacja to zaznacz to pole</span>
</div>
<div class=col-xs-4>

<div class="col-xs-5">
<input type=checkbox name=auction>
</div>
</div>




</div>


<br>
<div class=row>
<div class=col-xs-2>
</div>
<div class=col-xs-3>
<span class=form>Wstaw zdjęcie</span>
</div>
<div class=col-xs-4>

<div class="col-xs-5">
{{Form::file('image')}}
</div>
</div>

<br><br>
<div class=row>
<div class=col-xs-10>
<div align=center>
<input type=submit class = "btn btn-primary" value=dodaj>
</div>
</div>
</div>
<input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>
<div id="message"></div>
</div>
<?php $messages = $errors->all('<p style="color:red">:message</p>') ?>
<?php foreach ($messages as $msg): ?>
<div align=center><?= $msg ?></div>
<?php endforeach; ?>
@endsection
