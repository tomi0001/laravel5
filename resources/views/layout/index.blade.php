    <?php
    
    use View;
    use Auth;
    $permissions = new \App\Http\Controllers\klasa();
    $object_category = new \App\Http\Controllers\funkcje();
    $object_views = new \App\Http\Controllers\views();
    ?>
    <link href="{{ asset('./bootstrap-3/css/bootstrap.min.css') }}" rel="stylesheet"> 
    <script src="http://malsup.github.com/jquery.form.js"></script>
    <link href="{{ asset('./style.css') }}" rel="stylesheet"> 
    <script src="{{asset('./jquery.js')}}"></script>
    <script src="{{asset('./funkcje.js')}}"></script>
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="./bootstrap-3/js/bootstrap.min.js"></script>
    
<div class=container>
 <div class=row>
  <div class="col-xs-2" id=kolumna>
  <img src={{asset('./logo.gif')}}>
  </div>
  <div class="col-xs-10"  id=kolumna>
    <br>
    <div class=breadcrumb>
    <?php
    
    
    if (!Auth::check() ==true): ?>
    <li><a href="/registration">Rejestracja</a></li>
    
    <?php endif;?>
    

    <?php if (!empty(Auth::User()->login) ): 
    $result = $permissions->check_permissions(Auth::User()->permissions,"views_statistics");
    if ($result ==true):?>
    <li><a href="#">Statystyki</a></li>
    
  
    <?php endif; endif; ?>
    
    
        <?php if (!empty(Auth::User()->login) ): 
    $result = $permissions->check_permissions(Auth::User()->permissions,"change_setting");
    if ($result ==true):?>
    <li><a href="#">Moje konto</a></li>
    
  
    <?php endif; endif; ?>

            <?php if (!empty(Auth::User()->login) ): 
    $result = $permissions->check_permissions(Auth::User()->permissions,"views_user");
    if ($result ==true):?>
    <li><a href="#">Użytkownicy</a></li>
    
  
    <?php endif; endif; ?>
    
    
            <?php if (!empty(Auth::User()->login) ): 
            
    $result = $permissions->check_permissions(Auth::User()->permissions,"add_categories");
    if ($result ==true):?>
    <li><a href="/add_categories">Dodaj nową kategorie</a></li>
    
  
    <?php endif; endif; ?>
    
    
            <?php if (!empty(Auth::User()->login) ): 
    $result = $permissions->check_permissions(Auth::User()->permissions,"add_product");
    if ($result ==true):?>
    <li><a href="/add_product">Sprzedaj produkt</a></li>
    
  
    <?php endif; endif; ?>
    
    
    </div>
    
    <div class=breadcrumb>
    <?php
    
    $table = $object_category->show_categories_at_menu();
    $object_views->show_cate($table);
    
    
    ?>
    
    </div>
  </div>
 </div>
 <div class=row>
  <div class="col-xs-2" id=kolumna2>
  <br>
  
  <?php
  
    if (!empty($id)) {
      $table = $object_category->show_categories2_at_menu($id);
      $object_views->show_cate2($table);  
      $file = fopen("./menu","w+");
      fwrite($file,$id);
      //print "<font color=red>pupa</font>";
    }
    else {
      $file = file("./menu");
      $table = $object_category->show_categories2_at_menu($file[0]);
      $object_views->show_cate2($table);      
    }
    
  
  
  ?>
  
  
  
  
  
  <div class="well well-sm">
  <form class=form-inline>
  <div class="form-group">
  
  <input type=text class=form-control size=14 placeholder=szukaj>
  
  </div>
  <div align=center><button type=submit class="btn btn-warning">Szukaj</button></div>
  </form>
  </div>
  <?php if (Auth::check() ==true ): ?>
  
  <div align=center><button onclick=wyloguj() class="btn btn-danger">Wyloguj <?php print Auth::user()->login ?></button></div>
  
  <?php else:?>
  
  
  
  <div class="well well-sm">
  <form class=form-inline action=/login method=post>
  {!! csrf_field() !!}
  <div class="form-group">
  
  <input type=text class=form-control size=14 placeholder=login name=login>
  <input type=password class=form-control size=14 placeholder=haslo name=password>
  
  </div>
  <div align=center><button type=submit class="btn btn-warning">Zaloguj</button></div>
  </form>
  
  </div>
  <?php endif; ?>
  </div>
  
  <div class="col-xs-10">
  @yield('content')
  </div>
 </div>
</div>