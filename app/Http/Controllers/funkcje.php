<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use DB;
use File;
use Input;
use Auth;
class funkcje extends BaseController {
  public $error;
  
  public function show_categories() {
    $table = array();
    $i = 0;
    $bases = DB::select("select id,name from category");
    foreach($bases as $bases2) {
      $table[$i][0] = $bases2->id;
      $table[$i][1] = $bases2->name;
      $i++;
    }
    
    
    return $table;
  }
  
  
  public function show_categories_at_menu() {
    $table = array();
    $i = 0;
    $bases = DB::select("select id,name from category order by rand() limit 6");
    foreach($bases as $bases2) {
      $table[$i][0] = $bases2->id;
      $table[$i][1] = $bases2->name;
      $i++;
    }
    return $table;
  }
  
  public function show_categories2_at_menu($id) {
    $table = array();
    $i = 0;
    $bases = DB::select("select id,name from category2 where id_category = '$id' order by rand() limit 6");
    foreach($bases as $bases2) {
      $table[$i][0] = $bases2->id;
      $table[$i][1] = $bases2->name;
      $i++;
    }
    return $table;
  }
  
  
  public function save_categories($nama_cate) {
    
    $check_cate = DB::select("select name from category where name = '$nama_cate' limit 1");
    foreach( $check_cate as $check_cate2) {
	if ($check_cate2->name != "") return false;
    }
    
    
    //else {
    //print $nama_cate;
      DB::insert("insert into category(name) values('$nama_cate')");
      return true;
    //}
    
    //print "down";
  }
  
  public function scal_image($path_to_image) {
      $result3 = file_exists($path_to_image);
      if ($result3 == false) return false;
      $dane_obrazka = GetImageSize($path_to_image);
      if ($dane_obrazka[0] >= $dane_obrazka[1]) {
	$result = $dane_obrazka[0] / $dane_obrazka[1];
	if ($result >= 1 and $result < 1.8) $variable = 200;
	else $variable = 150;
      }
      else {
	$result = $dane_obrazka[1] / $dane_obrazka[0];
	$variable = 150;
      }
      if ($result > 4) $result2 = $result * ($variable / 10);
      else $result2 = $result * $variable;
  
    return array($result2,$dane_obrazka[0],$dane_obrazka[1]);
  }
  
  public function views_product($id) {
    $obiekt = new \App\Http\Controllers\views();
    
    if ($id != 0) {
      $result = DB::select("select name,price,description,auction,sold,quantity,path_to_image from products where id_category2 = '$id' and sold = ''");
    }
    else {
      $result = DB::select("select name,price,description,auction,sold,quantity,path_to_image from products where sold = ''");
    }
    $i = 0;
    
    foreach ($result as $result2) {
    $path_to_image = "./files/_mini_" . $result2->path_to_image;
    
      //$image = asset($path_to_image);
      //if ($path_to_image != "./files/_mini_2bfa355ff2b806373c2c48d347123711" and $path_to_image != "") {
      $result22 = $this->scal_image($path_to_image);
    //}
    //else {
      //$result22[0]  ="";
      //$result22[1]  ="";
      //$result  ="";
      //$result22[2] = "";
    //}
    
    
      $string = substr($result2->description,0,18);
      $string = $string . "...";
      $name = $result2->name;
      if ($result2->auction != 0) {
	$auction = "licytacja";
      }
      else {
	$auction = "";
      }
      $price = $this->calculate_price2($result2->price);
      $quantify = $result2->quantity;
      
      $result3 = $i % 2;
      if ($result3 == 0) {
	$obiekt->show_products($name,$price[0],$price[1],$string,$auction,$quantify,$path_to_image,1,$result22[0],$result22[1],$result22[2]);
	//$obiekt->show_products("result2->name","result2->price","string","result2->auction","result2->quantify","result2->path_to_image",1);
      }
      else {
      $obiekt->show_products($name,$price[0],$price[1],$string,$auction,$quantify,$path_to_image,2,$result22[0],$result22[1],$result22[2]);
	//$obiekt->show_products($result2->name,$result2->price,$string,$result2->auction,$result2->quantify,$result2->path_to_image,2);
      }
      $i++;
    }
  
  }
  
  public function save_categories2($id_cate,$name_cate) {
  
    $check_cate = DB::select("select name from category2 where id_category = '$id_cate' and name = '$name_cate' ");
    foreach ($check_cate as $check_cate2) {
      if ($check_cate2->name != "") return -1;
    }
    
    DB::insert("insert into category2(name,id_category) values('$name_cate','$id_cate')");
    return 0;
  
  
  
  }
  
  public function calculate_price($zl,$gr) {
    $new_gr = "";
    if (strlen($gr) == 1) $new_gr = "0" . $gr;
    if (strlen($gr) == 0) return $zl;
    else $new_gr = $gr;
    $new_price = $zl . "." . $new_gr;
    return $new_price;
  
  }
  
  public function calculate_price2($price) {
    if ( strstr($price,".") ) {
      $split = explode(".",$price);
      return $split;
    }
    return $price;
  
  }
  
  public function show_categories2($id) {
    $table = array();
    $i = 0;
    $bases = DB::select("select id,name from category2 where id_category = '$id' ");
    foreach($bases as $bases2) {
      $table[$i][0] = $bases2->id;
      $table[$i][1] = $bases2->name;
      $i++;
    }
    return $table;
  }
  
  
  public function check_flap($file) {
  
  
        //$product = new \App\Http\Controllers\funkcje();
     //$file = $request->file('image'); 
      
      //$name = $request->get("image");
      //print $name;
      $i = 0;
      //$enlargement = Input::get('image');
      //$enlargement2 = explode(".",$enlargement);
      if (!empty($file) ) {
       $ext = $file->guessExtension();

      }
      else $ext = "";
      //print $ext;
      //print $enlargement;
      $how_gr  = strlen(Input::get('gr'));
      
     if (Input::get('name') == "" or Input::get('description') == "" or Input::get('category') == "" or empty($file) or Input::get('zl') == "" and Input::get('gr') == "" or Input::get('quantity') == "" or Input::get('category2') == "" or $how_gr > 2 or ($ext != "jpeg" and $ext != "jpg" and $ext != "gif" and $ext != "png")) {
	if (Input::get('name') == ""    ) {
	  $validation[$i] = "Uzupełnij pole nazwa";
	}
	$i++;
	if (Input::get('description') == "") {
	  $validation[$i] = "Uzupełnij pole treść";
	}
	$i++;
	if (Input::get('category') == "") {
	  $validation[$i] = "Uzupełnij pole kategoria";
	}
	$i++;
	if (empty($file)) {
	  $validation[$i] = "Musisz wybrać zdjęcie";
	}
	$i++;
	if (Input::get('zl') == "" and Input::get('gr') == "") {
	  $validation[$i] = "Uzupełnij pole cena";
	}
	$i++;
	if (Input::get('quantity') == "") {
	  $validation[$i] = "Uzupełnij pole ilość";
	}
	$i++;
	if (Input::get('category2') == "") {
	  $validation[$i] = "Uzupełnij pole podrzędna categoria";
	}
	$i++;
	if ($how_gr > 2) {
	  $validation[$i] = "Podaj prawidłową wartość gr";
	}
	$i++;
	if ($ext != "jpeg" and $ext != "jpg" and $ext != "gif" and $ext != "png") {
	  $validation[$i] = "Rozszerzenia plików muszą mieć format [gif,png,jpeg]";
	}
	$this->error = 2;
	
     //return Redirect('add_product')->withErrors($validation)->withInput();
     return $validation;
     }
      else {
     $new_price = $this->calculate_price(Input::get('zl'),Input::get('gr'));
     
     $this->save_product($file,$new_price);
	}
  
  
  
  
  
  
  
  
  
  
  }
  
  
  
  
    public function decrease_file( $file, $width) {
    // pobieramy rozszerzenie pliku, na tej podstawie
    // korzystamy potem z odpowiednich funkcji GD
    $i = explode(".", $file);
    $file2 = "./files/_mini_" . $file;
    $file = "./files/" . $file;

    // rozszerzeniem pliku jest ostatni element tablicy $i
    $ext = end($i);

    // jeśli nie mamy jpega, gifa lub png zwracamy false.
    if($ext !== "jpg" &&
    $ext !== "gif" &&
    $ext !== "png" && $ext !== "jpeg") {
      return false;
    }

    // pobieramy rozmiary obrazka
    list($img_szer, $img_wys) = getimagesize($file);

    // obliczamy proporcje boków
    $proporcje = $img_wys / $img_szer;

    // na tej podstawie obliczamy wysokość
    $height = $width * $proporcje;

    // tworzymy nowy obrazek o zadanym rozmiarze
    // korzystamy tu z funkcji biblioteki GD
    // która musi być dołączona do twojej instalacji PHP,
    // najpierw tworzymy canvas.
    $canvas = imagecreatetruecolor($width, $height);
    $org = "";
    switch($ext) {
      case "jpg":
      $org = imagecreatefromjpeg($file);
      break;
      case "gif":
      $org = imagecreatefromgif($file);
      break;
      case "png":
      $org = imagecreatefrompng($file);
      case "jpeg":
      $org = imagecreatefromjpeg($file);
      break;
    }

    // kopiujemy obraz na nowy canvas
    imagecopyresampled($canvas, $org, 0, 0, 0, 0,$width, $height, $img_szer, $img_wys);
    //$canvas = imagerotate($canvas, 180, 0);

    // zapisujemy jako jpeg, jakość 70/100
    if(imagejpeg($canvas, $file2, 70)) {
      return true;
    } else {
      return false;
    }
  }
  

  public function save_product($file,$new_price) {
    $user = Auth::user()->login;
    $id_usera = DB::select("select id from users where login = '$user' ");
    $name = Input::get('name');
    $description = Input::get('description');
    $category = Input::get('category');
    $quantity = Input::get('quantity');
    $category2 = Input::get('category2');
    $auction =  Input::get("auction");
    $id_usera2 = $id_usera[0]->id;
    //print $id_usera[0]->id;
    $file_name = $this->save_file($file);
    if ($auction == "on") $auction = 1;
    else $auction = 0;
    DB::insert("insert into products(name,id_category,id_category2,price,description,auction,sold,id_usera,quantity,path_to_image) values('$name','$category','$category2','$new_price','$description','$auction','0','$id_usera2','$quantity','$file_name')");
    //$result = $this->save_file($file);
    //if ($result == true) print "dobrze";
    //else print "zle";
  
  }
  
  
  private function save_file($file) {

    $ext = $file->guessExtension();
    
    $crypt = rand(1,100) . rand(1,100) . rand(1,1000);
    $name = md5($crypt);
    $result = File::isFile("./files/" . $name . "." . $ext);
    
    while ($result !=  false) {
      $crypt = rand(1,100) . rand(1,100) . rand(1,1000);
      $name = md5($crypt);
      $result = File::isFile("./files/" . $name . "." . $ext);
    }
    if ($result == false) {
      $file->move('files', $name . '.' . $ext);
      $this->decrease_file( $name . "." . $ext, 200);
      return $name . "." . $ext;
    }
    else return false;
    //$file->move('files', 'newfilename.' . $a);
  
  
  }
  
  
}