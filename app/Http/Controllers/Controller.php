<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;
use Auth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Input;
use Illuminate\Support\Facades\Validator;
use Hash;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    
    public function index() {
    
          Auth::logout();
	  return View::make('/regi');
	  //print "dobrze";
    
    }
    public function ajax($id) {
    
     if (Auth::check()) {
    
      //if (Request::ajax()) {
      //return View('/ajax/ajax',['id' => 1]);
      return View('/ajax/ajax')->with('id',$id);
      //}
     }
    }
  public function add_product_succes() {

    $permissions = new \App\Http\Controllers\klasa();
    $product = new \App\Http\Controllers\funkcje();
  
    if (empty(Auth::User()->permissions)) {
      //print "dobrze";
      return Redirect('blad')->with('login_error','Nie masz dostępu do tej części strony');
    }
    else {
      $result = $permissions->check_permissions(Auth::user()->permissions,"add_product");
      if ($result == true) {
	
      }
    
    }
  
  
  
  
  
  
  
  
  }
  
  public function add_categories() {
  
    $permissions = new \App\Http\Controllers\klasa();
    if (empty(Auth::User()->permissions)) {
      //print "dobrze";
      return Redirect('blad')->with('login_error','Nie masz dostępu do tej części strony');
    }
    else {
      $result = $permissions->check_permissions(Auth::user()->permissions,"change_product");
      if ($result == true) {
	return View::make('add_categories');
      }
      else {
	return Redirect('blad')->with('login_error','Nie masz dostępu do tej części strony');
      }
  
  }
  
  }
  
  
  public function ajax3() {
    
      $permissions = new \App\Http\Controllers\klasa();
      $product = new \App\Http\Controllers\funkcje();
    if (empty(Auth::User()->permissions)) {
      //print "dobrze";
      return print "<div align=center class=\"alert alert-danger\" ><span>Wylogowałeś się musisz się ponownie zalogować</span></div></div>";
    }
    else {
  
       $result = $permissions->check_permissions(Auth::user()->permissions,"add_categories");
      if ($result == true) {
	if (Input::get('name') == "") print "<div align=center class=\"alert alert-danger\" ><span>Uzupełnij pole nazwa kategorii</span></div></div>";
	//return View::make('add_categories');
	//
	else {
	  $result2 = $product->save_categories(Input::get('name'));
	  if ($result2 == true) print "<div class=\"alert alert-success\">Pomyślnie dodano kategorie</span></div>";
	  else print "<div align=center class=\"alert alert-danger\" ><span>Już jest dana kategoria wybierz inną</span></div></div>";
	}
      }
            else {
	return print "<div align=center class=\"alert alert-danger\" ><span>Wylogowałeś się musisz się ponownie zalogować</span></div></div>";
      }
  
  //if (Request::ajax()) {
  //}
  
  }
  
  
  }
  
  
  public function ajax4() {
      $permissions = new \App\Http\Controllers\klasa();
      $product = new \App\Http\Controllers\funkcje();
    if (empty(Auth::User()->permissions)) {
      //print "dobrze";
      return print "<div align=center class=\"alert alert-danger\" ><span>Wylogowałeś się musisz się ponownie zalogować</span></div></div>";
    }
    else {
      $result = $permissions->check_permissions(Auth::user()->permissions,"add_categories");
      if ($result == true) {
	if (Input::get('category') == "" or Input::get('category2') == "") print "<div align=center class=\"alert alert-danger\" ><span>Uzupełnij pole nadrzędna kategoria i podrzędna kategoria</span></div></div>";
	else {
	  $result2 = $product->save_categories2(Input::get('category'),Input::get('category2'));
	  if ($result2 == 0) print "<div class=\"alert alert-success\">Pomyślnie dodano kategorie</span></div>";
	  if ($result2 == -1) print "<div align=center class=\"alert alert-danger\" ><span>Już jest dana kategoria  w kategorii nadrzędnej</span></div></div>";
	}
      }
      else {
	return print "<div align=center class=\"alert alert-danger\" ><span>Wylogowałeś się musisz się ponownie zalogować</span></div></div>";
      }
    
    }
  
  
  
  }
  
    
    public function ajax2(Request $request) {
          $permissions = new \App\Http\Controllers\klasa();
          $product = new \App\Http\Controllers\funkcje();
    //$user = new User();
    //print Auth::user()->permissions;
    if (empty(Auth::User()->permissions)) {
      //print "dobrze";
      return Redirect('blad')->with('login_error','Nie masz dostępu do tej części strony');
    }
    else {
    
      $result = $permissions->check_permissions(Auth::user()->permissions,"add_product");
      if ($result == true) {
      
      $file = $request->file('image'); 
      $result2 = $product->check_flap($file);
      if ($product->error == 2) {
      print "dobrzeee";
      return Redirect('add_product')->withErrors($result2)->withInput();
      }
      else return Redirect('add_product_succes');
      
      
      }
      else return Redirect('blad')->with('login_error','Nie masz dostępu do tej części strony');
      
    }

    
     
    
    //print $a;
     if (Auth::check()) {
    
      //if (Request::ajax()) {
      //return View('/ajax/ajax',['id' => 1]);
      //return View('/ajax/ajax2');
      //}
     }
    
    }
    public function registration() {
   if (!Auth::check()) {
    return View::make('/regi');
    }
    else {
    //print "dobrze";
      return View::make('blad');
    }
    
    
    }
    
    
    public function login() {
    
    
  $user = array(
    'login' => Input::get('login'),
    'password' => Input::get('password')
  );
  if (Input::get('login') == "" and Input::get('password') == "" ) {
    return Redirect('blad')->with('login_error','Uzupełnij pole login i hasło');
  }
  if (Auth::attempt($user))
  {
    return Redirect('/registration');
  }
  else {
    return Redirect('blad')->with('login_error','Nieprawidłowy login lub hasło');
  }
    
    }
        public function views_product() {
    
    //  $permissions = new \App\Http\Controllers\klasa();
    //$user = new User();
    //print Auth::user()->permissions;
    //if (empty(Auth::User()->permissions)) {
      //return Redirect('blad')->with('login_error','Nie masz dostępu do tej części strony');
    //}
    //else {
      //$result = $permissions->check_permissions(Auth::user()->permissions,"views_product");
      //if ($result == true) {
	return View::make('show_product');
      //}
      
      //else return Redirect('blad')->with('login_error','Nie masz dostępu do tej części strony');
    //}
    
    }
    
    public function views_product2($id) {
    
    //  $permissions = new \App\Http\Controllers\klasa();
    //$user = new User();
    //print Auth::user()->permissions;
    //if (empty(Auth::User()->permissions)) {
      //return Redirect('blad')->with('login_error','Nie masz dostępu do tej części strony');
    //}
    //else {
      //$result = $permissions->check_permissions(Auth::user()->permissions,"views_product");
      //if ($result == true) {
	return View::make('show_product')->with('id3',$id);
      //}
      
      //else return Redirect('blad')->with('login_error','Nie masz dostępu do tej części strony');
    //}
    
    }
    
    
    public function add_product() {
    
      $permissions = new \App\Http\Controllers\klasa();
  //$user = new User();
  //print Auth::user()->permissions;
    if (empty(Auth::User()->permissions)) {
      return Redirect('blad')->with('login_error','Nie masz dostępu do tej części strony');
    }
    else {
      $result = $permissions->check_permissions(Auth::user()->permissions,"add_product");
      if ($result == true) return View::make('add_product');
      else return Redirect('blad')->with('login_error','Nie masz dostępu do tej części strony');
    }
    
    }

    
    public function add_regi() {
    
              $rules = array(
    'email' => 'required|email|unique:users',
    'password' => 'required|same:password2',
    'login'=> 'required|unique:users'
      );
      $validation = Validator::make(Input::all(), $rules);
      if ($validation->fails())
      {
	return Redirect('registration')->withErrors($validation)->withInput();
      }
      $user = new \App\User;
      $user->email = htmlspecialchars(Input::get('email'));
      $user->password = Hash::make(Input::get('password'));
      $user->login = htmlspecialchars(Input::get('login'));
      $user->date_registration = time();
      $user->permissions = 1;
      if ($user->save())
      {
	//Auth::loginUsingId($user->id);
	return Redirect('registration_succes');
      }	
    
    }
    
    
    public function show_cate($id) {
      
      //print $id;
       return View('show_cate')->with('id',$id);
      
    }
    
}
